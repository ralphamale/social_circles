SocialThing::Application.routes.draw do

  resources :friend_circles

  resources :posts

  resources :links

  resources :friend_circle_memberships

  resources :users do
    collection do
      get "reset_password"
      get "fix_password"
    end
  end
  resource :session, only: [:new, :create, :destroy] do
    collection do
      get "forgot_password"
      put "send_email"
    end
  end
end
