class PostsController < ApplicationController
  def new
    @post = Post.new
    3.times { @post.links.build }

    @user_friend_circles = current_user.friend_circles

  end

  def create
    @post = Post.new(params[:post])
    @post.links.new(params[:link].values.compact)

    # if made :friend_circle_ids in the params hash via the form, can just grab
    # params[:post_share]
    @post.friend_circle_ids = params[:post_share][:friend_circle_id]

    # could skip post_share entirely because of @posts's has_many :through assoc, and then could just put :friend_circle_ids into the hash via the form

    # @friend_circle.member_ids = params[:friend_circle_memberships][:member_id]

#post_share[friend_circle_id][]
    @post.save!

    fail

  end

  def edit
  end

  def update
  end

  def show
  end

  def index
    @posts = [].tap do |posts_array|
      current_user.friend_circles.each do |circle|
        posts_array << circle.posts
      end
    end.flatten.uniq
    fail
  end

  def destroy
  end
end
