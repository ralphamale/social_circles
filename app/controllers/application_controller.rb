class ApplicationController < ActionController::Base
  protect_from_forgery

  helper_method :current_user


  def current_user
    return nil unless session[:session_token]
    @current_user ||= User.find_by_token(session[:session_token])
  end

  def login_user!(user)
    user.generate_token
    session[:session_token] = user.token
  end

end
