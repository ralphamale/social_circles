class UsersController < ApplicationController

  def new
    render :new
  end

  def create
    @user = User.new(params[:user])
    @post = Post.new(params[:post])
    @post.links.new(params[:link].values.compact)

    if @user.save
      @post.author_id = @user.id
      @post.save!
      fail
      login_user!(@user)
      flash[:notice] = "User successfully created"
      redirect_to user_url(@user)
    else
      flash[:errors] = "There was a problem"
      render :new
    end
  end

  def reset_password
    @user = User.find_by_forgot_password_token(params[:forgot_password_token])

    if @user.nil?
      render :json => "go away hacker"
    else
      render :reset_password
    end

  end

  def fix_password
    @user = User.find_by_email(params[:user][:email])
    @user.password = params[:user][:password]
    @user.save!
    redirect_to new_session_url
  end


  def edit
  end

  def update
  end

  def index
  end

  def show
    render :show
  end

  def destroy
  end

end
