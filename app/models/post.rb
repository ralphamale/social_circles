class Post < ActiveRecord::Base
  attr_accessible :author_id, :body, :title, :link_attributes

  has_many :post_shares, inverse_of: :post

  has_many :links, inverse_of: :post

  has_many :friend_circles, through: :post_shares


  belongs_to :author,
  class_name: "User",
  foreign_key: :author_id
end
