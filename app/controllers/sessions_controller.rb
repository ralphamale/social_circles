class SessionsController < ApplicationController

  def new
    render :new
  end

  def create
    user = User.find_by_credentials(
    params[:user][:email],
    params[:user][:password]
    )

    if user.nil?
      # make teh errorz
      render :new
      return
    else
      login_user!(user)
      redirect_to user_url(user.id)
    end
  end

  def destroy
    current_user.generate_token
    session[:session_token] = nil
    redirect_to new_session_url
  end

  def forgot_password

  end

  def send_email
    @user = User.find_by_email(params[:user][:email])
    if @user.nil?
      render :forgot_password
    else
      @user.generate_forgot_token
      msg = UserMailer.forgot_password(@user)
      msg.deliver!
    end
  end
end
