class User < ActiveRecord::Base
  attr_accessible :email, :password_digest, :password, :token

  validates :email, :password_digest, :token,
  presence: true
  validates :email, uniqueness: { message: " That email is taken! "}
  after_initialize :ensure_token
  # don't validate password  EVERY TIME. doofus.
  validates :password, length: { minimum: 6, allow_nil: true }, on: :create

  has_many :managed_circles,
  foreign_key: :owner_id

  has_many :friend_circle_memberships,
  foreign_key: :member_id

  has_many :friend_circles,
    through: :friend_circle_memberships;



  def password=(password)
    @password = password
    self.password_digest = BCrypt::Password.create(password).to_s
  end

  # must define this alongside password=
  def password
    @password
  end

  def is_password?(password)
    BCrypt::Password.new(self.password_digest).is_password?(password)
  end

  def generate_token
    self.token = SecureRandom.urlsafe_base64(16)
    self.save!
  end

  def generate_forgot_token
    self.forgot_password_token = SecureRandom.urlsafe_base64(16)
        self.save!
  end

  def ensure_token(force = true)
    self.token ||= SecureRandom.urlsafe_base64(16)
    self.save!
  end

  def self.find_by_credentials(email, password)
    user = User.find_by_email(email)

    user.is_password?(password) ? user : nil

  end

end
