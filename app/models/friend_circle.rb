class FriendCircle < ActiveRecord::Base
  attr_accessible :owner_id, :member_ids

  has_many :post_shares

  has_many :posts,
  through: :post_shares

  belongs_to :owner,
  class_name: "User",
  foreign_key: :owner_id

  has_many :friend_circle_memberships

  has_many :members,
  through: :friend_circle_memberships


end
