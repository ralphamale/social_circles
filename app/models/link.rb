class Link < ActiveRecord::Base
  attr_accessible :post_id, :url
  validates :post, presence: true

  belongs_to :post
end
