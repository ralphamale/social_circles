class UserMailer < ActionMailer::Base
  default from: "ralf@yden.com"

  def forgot_password(user)
    @user = user
    @url = "http://localhost:3000/users/reset_password?forgot_password_token=#{@user.forgot_password_token}"
    mail(to: user.email, subject: 'Forgot password?')
  end

end
