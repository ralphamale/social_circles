class FriendCirclesController < ApplicationController
  def new
    @users = User.all
    @owner = current_user
    @friend_circle = FriendCircle.new
    @current_members = []
  end

  def create
    @friend_circle = FriendCircle.new(params[:friend_circle])

    @friend_circle.member_ids = params[:friend_circle_memberships][:member_id]

    @friend_circle.save!

  end

  def edit
    @friend_circle = FriendCircle.includes(:friend_circle_memberships).find(params[:id])
    @users = User.all
    @current_members = FriendCircleMembership
      .where("friend_circle_memberships.friend_circle_id = ?", @friend_circle.id)
      .pluck(:member_id)

#:friend_circle_id, :member_id

  end

  def update
    @friend_circle = FriendCircle.find(params[:id])
    @friend_circle_membership = @friend_circle.friend_circle_memberships

    @current_members = FriendCircleMembership
      .where("friend_circle_memberships.friend_circle_id = ?", @friend_circle.id)
      .pluck(:member_id)

      @friend_circle.update_attributes!(params[:friend_circle_memberships])
 #   @friend_circle.

#    if params[:friend_circle_memberships] # ==> member_ids
  end

  def show
  end

  def index
  end

  def destroy
  end
end
